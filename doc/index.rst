Welcome to BATMAN's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 1

   readme_link
   changes_link
   contributing_link
   introduction
   settings
   tutorial
   space
   surrogate
   uq
   visualization
   pod
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

About
=====

See :ref:`about`.
